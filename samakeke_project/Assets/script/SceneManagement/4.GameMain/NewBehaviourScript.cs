using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField]
    private Button selectButton;

    void OnMouseEnter()
    {
        Debug.Log("Enter");
    }

    void OnMouseOver()
    {
        Debug.Log("MouseOver");
    }

    void OnMouseExit()
    {
        Debug.Log("Exit");
    }
}