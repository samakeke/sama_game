using UnityEngine;

public class FlashEffecter : MonoBehaviour
{
    [SerializeField]
    private Animator Panel;     // パネル点滅用

    public Animator GetPanel => this.Panel;
}