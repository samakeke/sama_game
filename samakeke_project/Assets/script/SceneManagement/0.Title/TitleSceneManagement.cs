using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleSceneManagement : MonoBehaviour
{
    #region Field

    [SerializeField]
    private Button start, option, load, delete;     // Start,Option,Load,Deleteボタン
    [SerializeField]
    private Text loadText;                          // テキスト表示用

    private FindSingleObj singleObj;                // 文字列を管理しているスクリプト型変数

    #endregion

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();

        // スタートボタン
        start.onClick.AddListener(() =>
        { 
            SceneManager.LoadScene(singleObj.GetSingleObjString.GetPlayerNameScene);
        });

        // オプションボタン
        option.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(singleObj.GetSingleObjString.GetOptionScene);
        });

        // ロードボタン
        load.onClick.AddListener(() =>
        {
            StaticPlayerName.PropertyPlayerName = PlayerPrefs.GetString(singleObj.GetSingleObjString.GetPrefsKey, "");

            if (StaticPlayerName.PropertyPlayerName == "")
            {
                StartCoroutine(TextSet(singleObj.GetSingleObjString.GetLoadErrorText));
            }
            else
            {
                SceneManager.LoadScene(singleObj.GetSingleObjString.GetCharaSelectScene);
            }
        });

        // デリートボタン
        delete.onClick.AddListener(() =>
        {
            if (PlayerPrefs.HasKey(singleObj.GetSingleObjString.GetPrefsKey) == false)
            {
                StartCoroutine(TextSet(singleObj.GetSingleObjString.GetDeleteErrorText));
            }
            else
            {
                PlayerPrefs.DeleteKey(singleObj.GetSingleObjString.GetPrefsKey);

                StartCoroutine(TextSet(singleObj.GetSingleObjString.GetDeleteText));
            }
        });
    }

    IEnumerator TextSet(string str)
    {
        loadText.text = str;

        yield return new WaitForSeconds(1.0f);

        loadText.text = "";
    }
}