using System;
using System.Collections;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    [SerializeField]
    private Button _button1;
    [SerializeField]
    private Button _button2;

    ObservableYieldInstruction<Button> y;

    void Start()
    {
        StartCoroutine(WaitButtonClick(_button1, _button2));
    }

    IEnumerator WaitButtonClick(Button button1, Button button2)
    {
        IObservable<Button> s = Observable.Merge(button1.OnClickAsObservable().Select(_ => button1),
                                                 button2.OnClickAsObservable().Select(_ => button2));
        y = s.First().ToYieldInstruction();
        
        yield return y;
        
        Debug.LogFormat("{0} がクリックされました", y.Result.name);

        switch(y.Result.name)
        {
            case "Button1":
                Debug.Log("ボタン1が押されたよ");
                break;
            case "Button2":
                Debug.Log("ボタン2が押されたよ");
                break;
        }
    }
}